package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var f mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("Topic: %s\n", msg.Topic())
	fmt.Printf("Message: %s\n", msg.Payload())
}
var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	fmt.Println("Connected")
}

var connectionLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	fmt.Printf("Connection Lost: %s\n", err.Error())
}

type timesync struct {
	Timesrv int64 `json:"timesync"`
}

func main() {
	mqtt.DEBUG = log.New(os.Stdout, "", 0)
	mqtt.ERROR = log.New(os.Stdout, "", 0)
	opts := mqtt.NewClientOptions().AddBroker("tcp://127.0.0.1:1883") //("newsmartfarm-nlb-4b5fa463f895b689.elb.ap-southeast-1.amazonaws.com:1883")
	//opts.SetUsername("sreeya")
	//opts.SetPassword("hardtmann")
	//opts.SetClientID("device01")
	opts.SetKeepAlive(5 * time.Second)
	opts.SetPingTimeout(1 * time.Second)
	opts.OnConnect = connectHandler
	opts.OnConnectionLost = connectionLostHandler

	c := mqtt.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	if token := c.Subscribe("v1/timesync", 0, nil); token.Wait() && token.Error() != nil {
		fmt.Println("Error!")
		fmt.Println(token.Error())
		os.Exit(1)
	}

	ticker := time.NewTicker(time.Millisecond * 1000)

	go func() {
		for timestamp := range ticker.C {
			time := timesync{Timesrv: timestamp.Unix()}
			var timedata []byte
			timedata, err := json.Marshal(time)
			if err != nil {
				fmt.Println(err)
			}
			token := c.Publish("v1/timesync", 0, false, timedata)
			token.Wait()
		}
	}()

	time.Sleep(24000 * time.Hour)
	c.Disconnect(250)
}
